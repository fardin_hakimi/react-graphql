# React-graphql

This is a very basic full-stack app demonstrating how to run graphql, react, and mongo together. The main intention of creating this app was to try out graphql apollo-server and functional react components.

#### How to run this app on your computer. 

The easiet way is to use the docker-compose file.  Run this command in the root of the main dir (docker-compose up -d --build)

Then you can access the frontend app at: localhost:8080