import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register'
import Dashboard from './pages/UserDashboard'
import Workspace from './component/Workspace'
import Navbar from './component/Navbar'
import { BrowserRouter as Router, Link, Switch , Route } from 'react-router-dom'

function App(props) {

  return (
    <Router>
      <div className="App">
        <Navbar/>
      <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/register">
            <Register />
          </Route>
          <Route path="/dashboard">
            <Dashboard/>
          </Route>
          <Route path="/workspaces/:workspaceId" render={ ({ match }) => <Workspace match={ match }/>}/>
          <Route path="/">
            <Home />
          </Route>
      
        </Switch>
      </div>
    </Router>
  )
}

export default App