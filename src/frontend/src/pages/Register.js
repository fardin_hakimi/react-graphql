import React, { useState } from 'react'
import { Form , Button, Alert } from 'react-bootstrap'
import { isEmpty } from '../utils/util'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import { Link } from 'react-router-dom'

export default function(props) {

    const [ name, setName ] = useState('')
    const [ username, setUsername ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ formIsValid, setFormIsValid ] = useState(true)
    const [ registerSuccess, setRegisterSuccess ] = useState(false)

    const REGISTER_USER = gql`
        mutation RegisterUser($name: String!, $username: String!, $password: String!){
            register (input: { username: $username, name: $name, password: $password }) {
                username,
                name,
                token
            }
        }
    `
    const [registerUserRemote, { registerUserResponse } ] = useMutation(REGISTER_USER, {
        onCompleted({register}) {
            setRegisterSuccess(true)
        },
        onError(error) {
            console.log("MUTATION ERROR =>", error)
        }
    })

    const functionMap = {
        'name': setName,
        'username': setUsername,
        'password': setPassword,
    }

    const handleChange = (event) => {
        event.preventDefault()
        functionMap[event.target.name](event.target.value)
    }

    const registerUser = async (event) => {

        event.preventDefault()
        
        if(isEmpty(name) || isEmpty(username) || isEmpty(password)) {
            setFormIsValid(false)
            setTimeout(() => {
                setFormIsValid(true)
            },10000)
        } else {
            registerUserRemote({variables: { name, username, password }})
        }
    }


    if(registerSuccess) {

        return (
            <Alert variant="success">
                <Alert.Heading>Hi there, thanks for signing up.</Alert.Heading>
                    <p>
                       continue to login with your shiny new account <Link to="/"> Here </Link>
                    </p>
            </Alert>
        )

    } else {

        return (
            <div className="container">
    
                
                {
                    !formIsValid && <Alert variant="danger" onClose={() => setFormIsValid(true)} dismissible>
                                        <p> All the form fields are required!</p>
                                     </Alert>
                }
    
                <Form onSubmit={registerUser}>
                    <Form.Group controlId="formBasicName">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" name="name" onChange={handleChange} value={name} placeholder="Enter Name" />
                    </Form.Group>
    
                    <Form.Group controlId="formBasicUsername">
                        <Form.Label>Username</Form.Label>
                        <Form.Control type="text" name="username" onChange={handleChange} value= {username} placeholder="Enter Username" />
                    </Form.Group>
    
                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" name="password" onChange={handleChange} value={password} placeholder="Password" />
                    </Form.Group>
    
                    <Button variant="primary" type="submit">
                        Register
                    </Button>
                </Form>
            </div>
        )
    }

}