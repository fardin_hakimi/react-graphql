import React from 'react'
import { Jumbotron } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { isUserLoggedIn, getUser } from '../utils/util'


const user = getUser()

function Home() {
  return (
      <div className="home">
          <Jumbotron>
              <h1>Hello, there!</h1>
              
              <p>

                { !isUserLoggedIn() ? <Link to="/login"> Login to continue </Link> : <Link to="/dashboard"> Continue to your dashboad { user.username }! </Link>}
                
              </p>
          </Jumbotron>
      </div>
  )
}

export default Home