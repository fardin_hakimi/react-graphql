import React, { useEffect, useState } from 'react'
import gql from 'graphql-tag'
import { Tab, Row, Nav, Col } from 'react-bootstrap'
import Workspaces from '../component/WorkspaceList'
export default function(props) {

    return (
        <Tab.Container id="left-tabs-example" defaultActiveKey="first">
            <Row>
                <Col sm={3}>
                    <Nav variant="pills" className="flex-column">
                        <Nav.Item>
                        <Nav.Link eventKey="first">Workspaces</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                        <Nav.Link eventKey="second">Settings</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Col>
                <Col sm={9}>
                    <Tab.Content>
                        <Tab.Pane eventKey="first">
                            <Workspaces/>
                        </Tab.Pane>
                        <Tab.Pane eventKey="second">
                            <h1> Settings ... </h1>
                        </Tab.Pane>
                    </Tab.Content>
                </Col>
            </Row>
        </Tab.Container>
    )
}