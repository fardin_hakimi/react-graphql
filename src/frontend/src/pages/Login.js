import React, { useState } from 'react'
import { Form , Button, Alert } from 'react-bootstrap'
import { isEmpty } from '../utils/util'
import { useMutation } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { CURRENT_USER_LOGGED_IN } from '../constants'
import { useHistory } from "react-router-dom";

export default function(props) {

    const [ username, setUsername ] = useState('')
    const [ password, setPassword ] = useState('')
    const [ formIsValid, setFormIsValid ] = useState(true)
    let history = useHistory()



    const LOGIN_USER = gql`
        mutation LoginUser($username: String!, $password: String!){
            login (input: { username: $username, password: $password }) {
                username,
                name,
                token,
                workspaces {
                    name
                }
            }
        }`

    const [ loginUserRemote, { loginUserRemoteResponse } ] = useMutation(LOGIN_USER, {
        onCompleted({login}) {
            if(login) {
                localStorage.setItem(CURRENT_USER_LOGGED_IN, JSON.stringify(login))
                history.push('/dashboard')
            } else {
                alert('This user does not exist!')
            }
            
        },
        onError(error) {
            console.log("LOGIN MUTATION FAILED =>", error)
        }
    })

    const functionMap = {
        'username': setUsername,
        'password': setPassword
    }

    const loginUser = (event) => {

        event.preventDefault()

        if(isEmpty(username) || isEmpty(password)) {
            setFormIsValid(false)
            setTimeout(() => {
                setFormIsValid(true)
            },10000)
        } else {
            loginUserRemote({
                variables: {
                    username,
                    password
                }
            })
        }
    }

    const handleChange = (event) => {
        event.preventDefault()
        functionMap[event.target.name](event.target.value)
    }

    return (
        <div className="container">

            {
                !formIsValid && <Alert variant="danger" onClose={() => setFormIsValid(true)} dismissible>
                                    <p> All the form fields are required!</p>
                                </Alert>
            }

            <Form onSubmit={loginUser}>
                <Form.Group controlId="formBasicUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control type="text" name="username" onChange={handleChange} value={username} placeholder="Enter Username" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" name="password" onChange={handleChange} value={password} placeholder="Password" />
                </Form.Group>

                <Button variant="primary" type="submit">
                    Login
                </Button>
            </Form>
        </div>
    )
}