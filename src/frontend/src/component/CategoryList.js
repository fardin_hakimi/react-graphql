import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { Spinner } from 'react-bootstrap'
import CategoryForm from './CategoryForm';
import TableComponent from './TableComponent';

const USER_CATEGORIES = gql`
        query GetCategoriesByWorkspace($workspaceId: String!) {
            getCategoriesByWorkspace(input: { workspaceId: $workspaceId }) {
                _id
                name
            }
        }
    `
export default function({ workspaceId }) {
    
    const { loading, error, data } = useQuery(USER_CATEGORIES, 
        {
            variables: {
                workspaceId
            }
        }
    )

    const relativePath = {
        path:'category',
        label: "view category"
    }

    const fields = [
        {
            name: "name",
            label: "Name"
        },
        {
            label: "Actions",
            name: "urls",
            type: "Link"
        }
    ]
      
    if( loading ) {
        return <Spinner animation="grow" />
    } else if (error) {
        return <h1> Something went wrong! contact the Dev team ( fardin ) </h1>
    } else if(data){
        
        return (
            <div className="container">
                <CategoryForm workspaceId = { workspaceId }/>
                <hr></hr>
                {
                    data.getCategoriesByWorkspace 
                    && 
                    <TableComponent fields = { fields } relativePath = { relativePath } items = { data.getCategoriesByWorkspace }/>
                }
            </div>
        ) 
    }
}