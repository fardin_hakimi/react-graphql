import React from 'react'
import { Modal, Button, Form, Alert } from 'react-bootstrap'
import { isEmpty } from '../utils/util'

export default class PopUpForm extends React.Component {

    constructor(props) {

        super(props)

        this.state = {
            show: false,
            formIsValid: true,
            fieldList: props.fieldList
        }
    }

    handleClose = () => {
        this.setState({ ...this.state, show: false, formIsValid: true})
    }

    handleShow = () => {
        this.setState({ ...this.state, show: true})
    }

    handleChange = ( event ) => {
        event.preventDefault()
        let fieldList = this.state.fieldList
        fieldList[event.target.getAttribute('data-key')].value = event.target.value
        this.setState({...this.state, fieldList})
    }

    handleSubmit = ( event ) => {
        event.preventDefault()

        let isFormValid = true;

        this.state.fieldList.forEach(field => {

            if( field.isRequired && isEmpty(field.value)) {
                this.setState({...this.state, formIsValid: false})
                isFormValid = false
                return;
            }
        })

        if(isFormValid) {
            this.props.handleSubmit(this.state.fieldList, () => {
                this.setState({ ...this.state , show: false })
            })
        }
    }

    render() {

        return (
            <>
                <Button variant="outline-primary" className="pull-right" onClick= { (event) => { this.handleShow() }} >
                    { this.props.actionButtonName }
                </Button>

                <Modal show={this.state.show} onHide={ this.handleClose }>
                    <Modal.Header closeButton>
                        <div className="row">
                            <Modal.Title className={this.props.modalTitle.classes.join(' ')}> { this.props.modalTitle.label } </Modal.Title>
                        </div>

                        <div className="row">
                            {
                                !this.state.formIsValid && <Alert variant="danger" dismissible> <p> All the form fields are required!</p></Alert>
                            }
                        </div>
                    </Modal.Header>
                    <Form onSubmit = { this.handleSubmit }>
                        <Modal.Body>
                            {
                                this.state.fieldList.map((item, index) => {
                                    return (
                                        <Form.Group controlId={`formBasic${item.name}`}>
                                            <Form.Label>{ item.label }</Form.Label>
                                            <Form.Control type="text" name={`${item.name}`} data-key= { index } onChange={ (event) => this.handleChange(event) }  value={ item.value } placeholder = { item.placeholder } />
                                        </Form.Group>
                                    )
                                })
                            }
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={ this.handleClose }> Close </Button>
                            <Button variant="primary" type="submit"> { this.props.submitButtonName } </Button>
                        </Modal.Footer>
                    </Form>
                </Modal>
            </>
        )
    }
}