import React from 'react'
import { useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { Spinner } from 'react-bootstrap'
import WorkspaceForm from './WorkspaceForm';
import TableComponent from './TableComponent';

const USER_WORKSPACES = gql`
        {
            workspaces {
                _id,
                name,
                owner {
                    name
                }
            }
        }
    `
export default function(props) {
    
    const { loading, error, data } = useQuery(USER_WORKSPACES)

    const relativePath = {
        path:'workspaces',
        label: "Go to workspaces"
    }

    const fields = [
        {
            name: "name",
            label: "Name"
        },
        {
            label: "Actions",
            name: "urls",
            type: "Link"
        }
    ]

    if(data && data.workspaces != null && data.workspaces != undefined) {

        return (
            <div className="container">
                <WorkspaceForm/>
                <hr></hr>
                <TableComponent 
                    fields = { fields }
                    relativePath = { relativePath }
                    items = { data.workspaces }
                />
            </div>
        ) 
    } else if( loading ) {
        return ( <Spinner animation="grow" /> )
    } else if (error) {
        return <h1> Something went wrong! contact the Dev team ( fardin ) </h1>
    } else {
        return <h1> Nothing found here! </h1>
    }
}