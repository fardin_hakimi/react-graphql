import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import PopUpForm from './PopUpForm';


export default function(props) {

    const CREATE_WORKSPACE = gql`
        mutation CreateWorkspace($name: String!, $description: String!){
            createWorkspace (input: { name: $name, description: $description }) {
                name
            }
    }`
    const [ callbackReference, setcallbackReference ] = useState(undefined)

    const fields = [
        {
            label: 'Workspace Name',
            name: 'workspaceName',
            isRequired: true,
            placeholder: 'Enter a name',
            value: ''
        },
        {
            label: 'Workspace Description',
            name: 'workspaceDescription',
            isRequired: true,
            placeholder: 'Enter a description',
            value: ''
        }
    ]

    const [ createWorkspaceRemote, { createWorkspaceRemoteResponse } ] = useMutation(CREATE_WORKSPACE, {
        onCompleted({createWorkspace}) {

            if(callbackReference != undefined) {
                callbackReference()
            }

            window.location.reload()
        },
        onError(error) {
            console.log("CREATE_WORKSPACE MUTATION FAILED =>", error)
        }
    })

    const createWorkspace = ( fieldList , showReference ) => {
        createWorkspaceRemote({
            variables: {
                name: fieldList[0].value,
                description: fieldList[1].value,
            }
        })
        setcallbackReference(showReference)
    }

    return(
        <PopUpForm 
            fieldList = { fields } 
            handleSubmit = { createWorkspace }
            modalTitle = { { label: "Create a workspace", classes: ['custom-class-title']} }
            actionButtonName = { "Create workspace" }
            submitButtonName = { "Create" }
        />
    )
}