import React, { useState } from 'react'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'
import PopUpForm from './PopUpForm'

export default function(props) {

    const CREATE_CATEGORY = gql`
        mutation CreateCategory($name: String!, $description: String!, $workspaceId: String!){
            createCategory (input: { name: $name, description: $description, workspaceId: $workspaceId }){
                name
            }
        }`

    const [ callbackReference, setcallbackReference ] = useState(undefined)

    const fields = [
        {
            label: 'Category Name',
            name: 'categoryName',
            isRequired: true,
            placeholder: 'Enter a name',
            value: ''
        },
        {
            label: 'Category Description',
            name: 'categoryDescription',
            placeholder: 'Enter a description',
            value: ''
        }
    ]

    const [ createCategoryRemote, { createCategoryRemoteResponse } ] = useMutation(CREATE_CATEGORY, {

        onCompleted({createCategory}) {

            if(callbackReference != undefined) {
                callbackReference()
            }

            window.location.reload()
        },
        onError(error) {
            console.log("CREATE CATEGORY MUTATION FAILED =>", error)
        }
    })

    const createCategory = ( fieldList , showReference ) => {
        createCategoryRemote({
            variables: {
                name: fieldList[0].value,
                description: fieldList[1].value,
                workspaceId: props.workspaceId
            }
        })
        setcallbackReference(showReference)
    }

    return(
        <PopUpForm 
            fieldList = { fields } 
            handleSubmit = { createCategory }
            modalTitle = { { label: "Create a category", classes: ['custom-class-title']} }
            actionButtonName = { "Create category" }
            submitButtonName = { "Create" }
        />
    )
}