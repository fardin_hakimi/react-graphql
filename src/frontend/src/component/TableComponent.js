import React from 'react'
import { Table } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function({ relativePath, fields, items }) {

    if( items.length > 0 ) {

        return (
            <div className="container">

                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            { fields.map((heading, index) => {
                                return <th key={index}> { heading.label } </th>
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {
                            items.map((item, index) => {

                                return (
                                    <tr key={item.name}>
                                        {
                                            fields.map((field) => {
                                                if(field.type == 'Link') {
                                                    return(
                                                        <td key={index}> 
                                                            <Link to={`/${relativePath.path}/${item._id}`}> { relativePath.label } </Link> 
                                                        </td>
                                                    )
                                                } else {
                                                    return <td key={index}> {item[field.name]} </td>
                                                }
                                            })
                                        }
                                    </tr> 
                                )         
                            })
                        }
                    </tbody>
                </Table>
            </div>
        ) 
    } else {
        return <h1> Nothing found here! </h1>
    }
}