import React from 'react'
import { isUserLoggedIn } from '../utils/util'
import { Nav } from 'react-bootstrap'
import { CURRENT_USER_LOGGED_IN } from '../constants'
import { useHistory } from "react-router-dom";

export default function(props) {

    let history = useHistory()


    const getNavItems = () => {

        console.log(" IS USER LOGGED IN", isUserLoggedIn())

        if(!isUserLoggedIn()) {

            return [
                {
                    name: "Home",
                    path: "/"
                },
                {
                    name: "Register",
                    path: "/register"
                },
                {
                    name: "Login",
                    path: "/login"
                }
            ]

        } else {

            return [
                {
                    name: "Home",
                    path: "/"
                },
                {
                    name: "Dashboard",
                    path: "/dashboard"
                },
                {
                    name: "Logout",
                    onClick: function(event) {
                        event.preventDefault()
                        localStorage.removeItem(CURRENT_USER_LOGGED_IN)
                        history.push('/')
                    }
                }
            ]
        }
    }

    return(
        <div className="container-fluid">
            <Nav defaultActiveKey="/" as="ul">
                {
                    getNavItems().map((navItem) => {

                        if(navItem.onClick) {
                            return (<Nav.Item as="li" key = { navItem.name }>
                                        <Nav.Link onClick={navItem.onClick}>{navItem.name}</Nav.Link>
                                    </Nav.Item>
                            )
                        } else {
                            return (<Nav.Item as="li" key = { navItem.name }>
                                        <Nav.Link href={navItem.path}>{navItem.name}</Nav.Link>
                                    </Nav.Item>)
                        }
                        
                    })
                }
            </Nav>
        </div>
    )
}