import React, { useState } from 'react'
import { useQuery, useParams } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { Nav , Spinner, Row, Col, Tab } from 'react-bootstrap'
import CategoryList from './CategoryList';

const WORKSPACE = gql`
            query GetWorkSpaceById($workspaceId: String!) {
                getWorkspaceById(input: { workspaceId: $workspaceId }) {
                    name
                }
            }
    `
export default function({match}) {

    let { workspaceId } = match.params
    
    const { loading, error, data } = useQuery(WORKSPACE, {
        variables: { workspaceId }
    })

    if(data) {

        return (
            <Tab.Container id="left-tabs-example" defaultActiveKey="creators">

                <Row>
                    <Col sm={3}>
                        <Nav variant="pills" className="flex-column">
                            <Nav.Item>
                                <Nav.Link eventKey="creators"> Creators </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="courses"> Courses </Nav.Link>
                            </Nav.Item>
                            <Nav.Item>
                                <Nav.Link eventKey="categories"> Categories </Nav.Link>
                            </Nav.Item>
                        </Nav>
                    </Col>
                <Col sm={9}>
                <Tab.Content>
                        <Tab.Pane eventKey="creators">
                            <h1> Courses </h1>
                        </Tab.Pane>
                        <Tab.Pane eventKey="courses">
                            <h1> Courses </h1>
                        </Tab.Pane>
                        <Tab.Pane eventKey="categories">
                            <CategoryList workspaceId={workspaceId}/>
                        </Tab.Pane>
                    </Tab.Content>
                </Col>
            </Row>
        </Tab.Container>
        ) 

    } else if( loading ) {
        return ( <Spinner animation="grow" /> )
    } else if (error) {
        console.log("ERROR => ", error)
        return <h1> Something went wrong! contact the Dev team ( fardin ) </h1>
    } else {
        return <h1> Nothing found here! </h1>
    }
}