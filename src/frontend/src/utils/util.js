import { CURRENT_USER_LOGGED_IN } from '../constants'


export const isEmpty = (val) => {
    return  ( val == '' || val == null || val == undefined )
}


export const isUserLoggedIn = () => {
    const user = JSON.parse(localStorage.getItem(CURRENT_USER_LOGGED_IN))
    return user != null
}

export const getUser = () => {
    return JSON.parse(localStorage.getItem(CURRENT_USER_LOGGED_IN))
}