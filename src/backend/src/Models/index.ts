import { Schema, Document } from 'mongoose'
import * as mongoose from 'mongoose'
export interface Icategory extends Document {
  name: string;
  workspaceId: string,
  description?: string;
}
export interface Iuser extends Document {
  name: string;
  username: string;
  password: string;
}
export interface IUserLoginInput {
    username: string;
    password: string;
}
export interface Icourse extends Document {
  name: string;
  creatorId: string;
  categoryId: string;
  workspaceId: string;
  url?: string;
}
export interface IuserResponse {
    _id: string;
    name: string;
    username: string;
    token: string
}
export interface ICreator extends Document {
  name: string;
  rank?: number;
}
export interface Iworkspace extends Document {
  name: string;
  description: string;
  ownerId: string;
}

export interface Itoken extends Document {
  username: string;
  token: string;
}

const CategorySchema: Schema = new Schema({
  name: { type: String, required: true },
  workspaceId: { type: String, required: true},
  description: { type: String, required: false }
})

const CourseSchema: Schema = new Schema({
  name: { type: String, required: true },
  creatorId: { type: String, required: true },
  categoryId: { type: String, required: true },
  workspaceId: { type: String, required: true},
  url: { type: String, required: false }
})

const CreatorSchema: Schema = new Schema({
  name: { type: String, required: true },
  rank: { type: Number, required: false }
})

const UserSchema: Schema = new Schema({
        username: { type: String, required: true , unique: true },
        name: { type: String, required: true },
        password: { type: String, required: true }
})

const WorkspaceSchema: Schema = new Schema({
  name: { type: String, required: true , unique: true },
  description: { type: String, required: true },
  ownerId: { type: String, required: true }
})

const TokenSchema: Schema = new Schema({
  username: { type: String, required: true , unique: true },
  token: { type: String, required: true }
})

export const Token = mongoose.model<Itoken>('Token', TokenSchema)
export const Workspace = mongoose.model<Iworkspace>('Workspace', WorkspaceSchema)
export const User = mongoose.model<Iuser>('User', UserSchema)
export const Creator = mongoose.model<ICreator>('Creator', CreatorSchema)
export const Course  = mongoose.model<Icourse>('Course', CourseSchema)
export const Category = mongoose.model<Icategory>('Category', CategorySchema)