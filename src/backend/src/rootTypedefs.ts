import { gql } from 'apollo-server'


// The GraphQL schema
const rootTypeDefs = gql`
  type Query {
      courses:[Course!]
      category(name: String): Category
      categories: [Category]
      creators: [Creator]
      workspaces: [Workspace]
      users: [User]
      heartBeat: String
      getWorkspaceById(input: getWorkspaceByIdInput ): Workspace
      getCategoriesByWorkspace(input: GetCategoriesByWorkspaceInput ): [Category]
  }

  type Mutation {
      createCourse(input: CreateCourseInput ): Course
      createCategory(input: CreateCategoryInput ): Category
      createCreator(input: CreateCreatorTypeDef ): Creator
      createWorkspace(input: CreateWorkspaceInput ): Workspace
      login(input: LoginUserInput ): User
      register(input: RegisterUserInput ): User
  }
`

export default rootTypeDefs