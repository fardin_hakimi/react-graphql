import { User, Iuser, IUserLoginInput, Workspace, Iworkspace, IuserResponse, Token, Itoken } from '../../Models/index'

/**
 * Generate and return a token. for now it is just one dummy token.
 * @returns string
 */
export async function generateJwtToken(username:string) {

    const token = await Token.findOne({username})
    
    if(token) {
        return token.token
    } else {
        const newToken:Itoken = new Token({username, token: `${username}<>??!!_dummy_token_$))((!!!)):**))))))`})
        return (await newToken.save()).token
    }
}

/**
 * Validate a token against a token db. for now it is just 1 token!
 * @param token 
 */
export async function isTokenValid(username:string, token: string) {

    const tokenInstance:Itoken = await Token.findOne({username, token})

    if(tokenInstance != null) {
        return tokenInstance.token != null
    }

    return false
}

export default {

    Query: {

        users: async (
            parent: any,
            args:any,
            context: any,
            info: any):Promise<Iuser[]> => await User.find({})
    },

    Mutation: {

        register: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<IuserResponse> => {
                const { name, username, password }: Iuser = args.input

                const alreadyExists:Iuser = await User.findOne({ username })

                if(alreadyExists) {
                    return { _id: alreadyExists._id, username: alreadyExists.username, name: alreadyExists.name, token: await generateJwtToken(username)}
                } else {
                    const user:Iuser = new User({name, username, password})

                    try {
                        await user.save()
                        return { _id: user._id, username: user.username, name: user.name, token: await generateJwtToken(username)}
                    } catch (error) {
                        console.log(error)   
                    }
                }
            },

        login: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<IuserResponse>  => {
                const { username, password}: IUserLoginInput = args.input
                const user = await User.findOne({username, password})

                if(user) {
                    return {
                        _id: user._id,
                        name: user.name,
                        username: user.username,
                        token: await generateJwtToken(username)
                    }
                }
        }
    },

    User: {
        workspaces: async (
            parent: any,
            args: any,
            context: any,
            info: any):Promise<Iworkspace[]> => await Workspace.find({ownerId: parent._id})
    }
}