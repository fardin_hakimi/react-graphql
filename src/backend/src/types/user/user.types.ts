import { gql } from 'apollo-server'

const userTypeDefs = gql`

    type User {
        name: String!
        username: String!
        token: String!
        workspaces: [Workspace]
    }

    input LoginUserInput {
        username: String!
        password: String!
    }

    input RegisterUserInput {
        name: String!
        username: String!
        password: String!
    }
`

export default userTypeDefs