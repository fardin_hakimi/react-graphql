import { User, Iuser, Workspace, Iworkspace,
        Icategory, Category, Creator, 
        Course, ICreator, Icourse } from '../../Models/index'

export default {

    Query: {

        workspaces: async (
            parent: any,
            args:any,
            context: any,
            info: any):Promise<Iworkspace[]> => {

                const user:Iuser = await context.user

                if (user != null) {
                    const workspaces: Iworkspace[] = await Workspace.find({ownerId:user._id})
                    return workspaces
                } else {                
                    return Promise.reject("User is not logged")
                }
        },

        getWorkspaceById: async (
            parent: any,
            args:any,
            context: any,
            info: any):Promise<Iworkspace> => {

                const { input: { workspaceId } } = args

                const workspace:Iworkspace = await Workspace.findOne({_id: workspaceId})

                return workspace
            }
    },

    Mutation: {

        createWorkspace: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<Iworkspace> => {

                let { input: { name, description, ownerId } } = args

                const user:Iuser = await context.user

                if(user) {
                    ownerId = user._id
                }

                const workspace:Iworkspace = new Workspace({ name, description, ownerId})

                try {
                    return await workspace.save()
                } catch (error) {
                    console.log(error)
                }
            }
    },

    Workspace: {

        categories: async (
            parent: any,
			args:any,
			context: any,
            info: any):Promise<Icategory[]> => await Category.find({workspaceId: parent._id}),

        creators: async (
            parent: any,
            args: any,
            context: any,
            info: any): Promise<ICreator[]> => await Creator.find({workspaceId: parent._id}),

        courses: async (
            parent: any,
            args: any,
            context: any,
            info: any): Promise<Icourse[]> => await Course.find({workspaceId: parent._id}),

        owner: async (
            parent: any,
            args: any,
            context: any,
            info: any): Promise<Iuser> => await User.findOne({_id: parent.ownerId})
    }
}