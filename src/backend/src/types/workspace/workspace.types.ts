import { gql } from 'apollo-server'

const workspaceTypeDefs = gql`

    type Workspace {
        _id: String
        name: String!
        description: String!
        owner: User
        categories: [Category]
        courses: [Course]
        creators: [Creator]
    }
    
    input CreateWorkspaceInput {
        name: String!
        description: String!
        ownerId: String
    }

    input getWorkspaceByIdInput {
        workspaceId: String!
    }
`

export default workspaceTypeDefs