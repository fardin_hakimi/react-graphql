import { gql } from 'apollo-server'

const categoryTypeDefs = gql`

    type Category {
        name: String!
        description: String
        workspace: Workspace
        courses: [Course]
    }
    
    input CreateCategoryInput {
        name: String!,
        description: String
        workspaceId: String
    }

    input GetCategoriesByWorkspaceInput {
        workspaceId: String!
    }
`

export default categoryTypeDefs
