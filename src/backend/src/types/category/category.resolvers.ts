import { Category, Icategory, Course,
        Icourse, Workspace, Iworkspace } from "../../Models/index"
        
export default {

    Query: {

        category: async (
            parent: any,
			args:any,
			context: any,
            info: any):Promise<Icategory> => await Category.findOne({name: args.name})
        ,
        categories: async (
            parent: any,
			args:any,
			context: any,
            info: any):Promise<Icategory[]> => await Category.find({})
        ,

        getCategoriesByWorkspace: async (
            parent: any,
            args: any,
            context: any,
            info: any):Promise<Icategory[]> => {
                const { input: { workspaceId }} = args
                return await Category.find({workspaceId})
            }
    },

    Mutation: {
        createCategory: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<Icategory>  => {

                const { input: { name, description , workspaceId} } = args
                const category:Icategory = new Category({ name, description, workspaceId })

                try {
                    
                    category.save()

                } catch (error) {
                    console.log(error)
                }
                
                return category
        }
    },

    Category: {
        workspace: async (
            parent: any,
            args: any,
            context: any,
            info: any
        ): Promise<Iworkspace> => await Workspace.findOne({_id: parent.workspaceId}),

        courses: async (
            parent: any,
            args: any,
            context: any,
            info: any
        ): Promise<Icourse[]> => await Course.find({categoryId: parent._id})
    }
}