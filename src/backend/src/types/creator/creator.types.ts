import { gql } from 'apollo-server'

const creatorTypeDefs = gql`

    type Creator {
        name: String!
        workspace: Workspace
        courses: [Course]
        rank: Int
    }

    input CreateCreatorTypeDef {
        name: String!,
        rank: Int
    }
`

export default creatorTypeDefs