import { ICreator, Creator, Course, Icourse, Workspace, Iworkspace } from '../../Models/index'

export default {

    Query: {

        creators: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<ICreator[]>  => await Creator.find({})
    },

    Mutation: {

        createCreator: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<ICreator>  => {
                
                const input: CreateCreatorType = args.input
                const creator: ICreator = new Creator({ name: input.name, rank: input.rank })

                try {

                    await creator.save()
                    
                } catch (error) {
                    console.log(error)
                }

                return creator
        }
    },

    Creator: {
        workspace: async (
            parent: any,
            args: any,
            context: any,
            info: any
        ): Promise<Iworkspace> => await Workspace.findOne({_id: parent.workspaceId}),

        courses: async (
            parent: any,
            args: any,
            context: any,
            info: any):Promise<Icourse[]> => await Course.find({creatorId: parent._id})
    }
}

export interface CreateCreatorType {
    name: string,
    rank?: number
}