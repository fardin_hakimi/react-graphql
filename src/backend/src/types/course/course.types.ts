import { gql } from 'apollo-server'

const courseTypeDefs = gql`

    type Course {
        name: String!
        creator: Creator!
        workspace: Workspace
        category: Category!
        url: String
    }

    input CreateCourseInput {
        name: String!
        creatorId: String!
        categoryId: String!
        url: String
    }
`

export default courseTypeDefs