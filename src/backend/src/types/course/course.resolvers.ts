import { Course, Icourse, Category, Icategory,
         Creator, ICreator, Workspace, Iworkspace } from "../../Models/index";

export default {


    Query: {

        courses: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<Icourse[]> => await Course.find({})
    },

    Mutation: {

        createCourse: async (
            parent: any,
			args:any,
			context: any,
			info: any):Promise<Icourse> => {

                const { input: { name, creatorId, categoryId, url } } = args

                const course:Icourse = new Course({ name, creatorId, categoryId, url})

                try {

                    return await course.save()
                    
                } catch (error) {
                    console.log(error)
                }
            }
    },

    Course: {

        workspace: async (
            parent: any,
            args: any,
            context: any,
            info: any
        ): Promise<Iworkspace> => await Workspace.findOne({_id: parent.workspaceId}),

        category: async (
            parent: any,
			args:any,
			context: any,
            info: any):Promise<Icategory> => {
                return await Category.findOne({_id: parent.categoryId})
            }
        ,
        creator: async(
            parent: any,
            args: any,
            context: any,
            info: any): Promise<ICreator> => {
                return await Creator.findOne({_id: parent.creatorId})
            }
    }
}