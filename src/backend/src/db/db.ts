
import { connect } from 'mongoose'

/**
 * connect to mongodb database; try after 1 sec if the first attempt fails
 */
export const connectWithRetry = () :void => {

    const dbHost = process.env.NODE_ENV !== 'dev' ? 'mongo': '0.0.0.0'
    const dbUrl :string = process.env.DATABASE_URL || `mongodb://root:root@${dbHost}:27017/?authSource=admin`
    connect(dbUrl, { useNewUrlParser: true })
    .then(() => {
        console.log('Db connection established!')
    })
    .catch((err) => {
        console.log(err)
        console.log("trying in 1 sec")
        setTimeout(connectWithRetry, 1000)
    })
}