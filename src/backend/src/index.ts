import { connectWithRetry } from './db/db'
import { ApolloServer } from 'apollo-server'
import  rootTypedefs from './rootTypedefs'
import { isTokenValid } from './types/user/user.resolvers'
// Course
import courseTypeDefs from './types/course/course.types'
import courseResolvers from './types/course/course.resolvers'

// Creator
import creatorTypeDefs from './types/creator/creator.types'
import creatorResolvers from './types/creator/creator.resolvers'
// Category
import categoryTypeDefs from './types/category/category.types'
import categoryResolvers from './types/category/category.resolvers'

// User
import userTypeDefs from './types/user/user.types'
import userResolvers from './types/user/user.resolvers'

// Workspace
import workspaceTypeDefs from './types/workspace/workspace.types'
import workspaceResolvers from './types/workspace/workspace.resolvers'
import { User, Iuser } from './Models';

connectWithRetry()

// A map of functions which return data for the schema.
const helloResolvers = {
  Query: {
    heartBeat: () => {
      return 'It is alive!'
    }
  },
}

export interface ContextInput {
	req: {
		headers: any,
		params: any,
		query: any,
		body: any,
		url: any,
		method: any,
		client: any
	}
}

const server = new ApolloServer({
  cors: {
    origin: '*',
    methods: '*'
  },
  typeDefs: [
    rootTypedefs,
    categoryTypeDefs,
    courseTypeDefs,
    creatorTypeDefs,
    userTypeDefs,
    workspaceTypeDefs
  ],
  resolvers: {
    Query: {
        ...helloResolvers.Query,
        ...courseResolvers.Query,
        ...categoryResolvers.Query,
        ...creatorResolvers.Query,
        ...userResolvers.Query,
        ...workspaceResolvers.Query
    },

    Mutation: {
      ...creatorResolvers.Mutation,
      ...categoryResolvers.Mutation,
      ...courseResolvers.Mutation,
      ...userResolvers.Mutation,
      ...workspaceResolvers.Mutation
    },

    Course: courseResolvers.Course,
    Creator: creatorResolvers.Creator,
    Category: categoryResolvers.Category,
    Workspace: workspaceResolvers.Workspace,
    User: userResolvers.User
  },
  context: async (requestContext: ContextInput):Promise<any> => {
    const { username, token } = requestContext.req.headers
  
    const isValid = await isTokenValid(username, token)
    if(isValid){
      const user = User.findOne({username})
      return { ...requestContext, ...{
        user: user
      }}
    }
    return { ...requestContext, ...{
      user: null
    }}
  }

})

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});